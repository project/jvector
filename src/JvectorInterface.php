<?php
/**
 * @file
 * Contains \Drupal\jvector\JvectorInterface.
 */

namespace Drupal\jvector;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a Jvector entity.
 */
interface JvectorInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}